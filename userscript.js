// ==UserScript==
// @name         LimeWire V1
// @namespace    http://tampermonkey.net/
// @version      1
// @description  Send url or youtube video to api for downloading.
// @author       James Burgess
// @match        https://www.youtube.com/watch?*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const $ = (s, x = document) => x.querySelector(s)
	const $el = (tag, opts) => {
		const el = document.createElement(tag)
		Object.assign(el, opts)
		return el
	}

    // create shadow dom
	const shadowHost = $el('div')
	const shadow = shadowHost.attachShadow ? shadowHost.attachShadow({ mode: 'closed' }) : shadowHost // no shadow dom

    // create event trigger
    var button = document.createElement("button");
    button.innerHTML = "Download to Drive";
    button.setAttribute("id", "limewire-download-button");
    button.setAttribute("class", "draw-border");
    shadow.appendChild(button)

    // callback handler to request to the API.
    button.addEventListener('click', function() {
        const bodyFormData = new FormData()
        bodyFormData.set('name', "James")
        bodyFormData.set('url', window.location)

        fetch("http://localhost:8080/yt", {
            method: "POST",
            body: bodyFormData,
            config: { headers: { 'Content-Type': 'multipart/form-data' }}
        }).then(res => {
            console.log("Request complete! response:", res);
            alert("Dowloading Song to drive!")
        }).catch(err => {
            console.log("err", err)
            alert("Error Downloading bruh")
        });
    });

    // attach shadow dom element to the page
	setInterval(() => {
		const el =
			$('#info-contents') ||
			$('#watch-header') ||
			$('.page-container:not([hidden]) ytm-item-section-renderer>lazy-list')
		if (el && !el.contains(shadowHost)) {
			el.appendChild(shadowHost)
		}
    }, 1000)

    // get stylish ya basic bish
    const stylish = `
.draw-border {
box-shadow: inset 0 0 0 4px #58afd1;
color: #58afd1;
transition: color 0.25s 0.0833333333s;
position: relative;
}
.draw-border::before, .draw-border::after {
border: 0 solid transparent;
box-sizing: border-box;
content: '';
pointer-events: none;
position: absolute;
width: 0;
height: 0;
bottom: 0;
right: 0;
}
.draw-border::before {
border-bottom-width: 4px;
border-left-width: 4px;
}
.draw-border::after {
border-top-width: 4px;
border-right-width: 4px;
}
.draw-border:hover {
color: #ffe593;
}
.draw-border:hover::before, .draw-border:hover::after {
border-color: #ffe593;
transition: border-color 0s, width 0.25s, height 0.25s;
width: 100%;
height: 100%;
}
.draw-border:hover::before {
transition-delay: 0s, 0s, 0.25s;
}
.draw-border:hover::after {
transition-delay: 0s, 0.25s, 0s;
}
#limewire-download-button {
background: none;
border: none;
cursor: pointer;
line-height: 1.5;
font: 700 1.2rem 'Roboto Slab', sans-serif;
padding: 1em 2em;
letter-spacing: 0.05rem;
}
#limewire-download-button:focus {
outline: 2px dotted #55d7dc;
}
`
    shadow.appendChild($el('style', { textContent: stylish }))
})();

