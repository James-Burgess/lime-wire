from bottle import run, request, response, post, hook
from subprocess import Popen, PIPE
import threading
import shlex
from loguru import logger


@post('/yt')
def hello():
    url = request.forms.get('url')
    user = request.forms.get('name')

    if not url and not user:
        logger.error("bad data")
        response.status = 400
        return "400 Bad Request"

    # download(url, user)
    download_thread = threading.Thread(target=download, args=(url, user,))
    download_thread.start()
    response.status = 200
    return "Downloading... Probabily"


@hook('after_request')
def enable_cors_after_request_hook():
    """
    This executes after every route. We use it to attach CORS headers when
    applicable.
    """
    add_cors_headers()


def add_cors_headers():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'POST'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'


class Result:
    pass


def cmd(command):
    result = Result()

    p = Popen(shlex.split(command), stdin=PIPE, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = p.communicate()

    result.exit_code = p.returncode
    result.stdout = stdout
    result.stderr = stderr
    result.command = command

    if p.returncode != 0:
        logger.info("Error executing command [%s]" % command)
        logger.info("stderr: [%s]" % stderr)
        logger.info("stdout: [%s]" % stdout)
        raise stderr

    return result


def download(url, user):
    logger.info(f"Downloading url: {url}")

    get_sng_cmd = f"""
    youtube-dl {url}
    -x
    --embed-thumbnail
    --add-metadata
    --output '~/Music/dl/{user}/%(artist)s%(title)s.%(ext)s'
    --metadata-from-title \"(?P<artist>.+?) - (?P<title>.+)\" \
    --audio-format mp3
    --audio-quality 0
    """

    try:
        res = cmd(get_sng_cmd)
    except Exception as e:
        return f"Error Downloading song {e}"

    dest = res.stdout.decode("utf-8")
    dest = dest.split(' Adding thumbnail to "')[1].replace('"\n', '')
    logger.info(f"song downloaded to local, {dest}")
    return dest


run(host='localhost', port=8080, debug=True)
